package com.ranginkaman.regexpattern;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    public static final Pattern PASSWORD_PATTERN = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$");

//    ^                 # start-of-string
//            (?=.*[0-9])       # a digit must occur at least once
//            (?=.*[a-z])       # a lower case letter must occur at least once
//            (?=.*[A-Z])       # an upper case letter must occur at least once
//            (?=.*[@#$%^&+=])  # a special character must occur at least once you can replace with your special characters
//            (?=\\S+$)          # no whitespace allowed in the entire string
//            .{4,}             # anything, at least six places though
//    $                 # end-of-string



    private EditText editTextEmail;
    private EditText editTextPass;
    private EditText editTextUser;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateEmail() && validatePassword()) {
                    Toast.makeText(MainActivity.this, "You are Logging", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private boolean validateEmail() {
        String emailInput = editTextEmail.getText().toString().trim();
        if (emailInput.isEmpty()) {
            editTextEmail.setError("Email is empty");
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()) {
            Toast.makeText(this, "Your Email Address is Not Valid", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    private boolean validatePassword() {
        String passwordInput = editTextPass.getText().toString().trim();
        if (passwordInput.isEmpty()) {
            editTextPass.setError("Password is empty");
            return false;
        } else if (!PASSWORD_PATTERN.matcher(passwordInput).matches()) {
            Toast.makeText(this, "Your Password is too Weak", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    public void bind() {
        editTextEmail = findViewById(R.id.edit_text_email);
        editTextUser = findViewById(R.id.edit_text_user);
        editTextPass = findViewById(R.id.edit_text_pass);
        button = findViewById(R.id.button_check);
    }
}
